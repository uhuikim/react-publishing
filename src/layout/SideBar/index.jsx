import React, { Component ,useState} from 'react'
import { Item ,Image, Button, Form, Accordion, Menu, List,Modal, Checkbox } from 'semantic-ui-react';



  
  const panels = [
    {
        key: 'category',
        title: {
        content:<> <Image className="inline-block" src="resources/icon/카테고리 메인.png"></Image>
        <span className="mgl10">카테고리 메인</span></>              
        },
        content: {
        content: (
            <List className="content_wrap">
            <List.Content>
                <List.Item>
                    <span className="mgr5 inline-block">-</span>
                    생활가전
                </List.Item>
                <List.Item>
                    <span className="mgr5 inline-block">-</span>
                    뷰티/메이크업
                </List.Item>
                <List.Item>
                    <span className="mgr5 inline-block">-</span>
                    잡화/패션
                </List.Item>
            </List.Content>
        </List>
        ),
        },
    },
    {
        key: 'photo-review',
        title: {
        content: <>
        <Image className="inline-block" src="resources/icon/베스트 포토 리뷰.png"></Image>
        <span className="mgl10">베스트 포토리뷰</span>
        </>
        },
        content: {
        content: (
            <span>
            </span>
        ),
        },
    },
                       {
        key: 'purchase-list',
        title: {
        content: <>
        <Image className="inline-block" src="resources/icon/구매내역.png"></Image>
        <span className="mgl10">구매내역</span>
        </>
        },
        content: {
        content: (
            <span>
            </span>
        ),
        },
    },

{
        key: 'pruduct-review',
        title: {
        content: <>
        <Image className="inline-block" src="resources/icon/상품리뷰.png"></Image>
        <span className="mgl10">상품리뷰</span>
        </>
        },
        content: {
        content: (
            <span>
            </span>
        ),
        },
    },
    ]
  
    function exampleReducer(state, action) {
        switch (action.type) {
          case 'close':
            return { open: false }
          case 'open':
            return { open: true }
          default:
            throw new Error('Unsupported action...')
        }
      }
 
const SideBar =()=>  {
    const [state, dispatch] = React.useReducer(exampleReducer, {
        open: false,
      })
      const { open } = state
   
    return (
        <>
            <div className="sidebar_wrap">
                <Item className="user_info">
                    <Item.Header  className="center mgb5">
                        <Image bordered className="inline-block mgb10" src="resources/images/Add Photos.png"></Image>
                        <p>신유니</p>
                    </Item.Header>
                    <Item.Meta className="mgb15">
                        <p className="inline">rachel@nextrr.io</p>
                    </Item.Meta>
                    <Item.Description  className="login-btn mgb15">
                        <Button basic onClick={() => dispatch({ type: 'open'})}>
                            로그인
                        </Button>
                    </Item.Description>
                </Item>

            <Accordion defaultActiveIndex={0} panels={panels} />
            </div>       
            
            
            <Modal
                className="login"
                closeIcon
                open={open}
                onClose={() => dispatch({ type: 'close' })}
                size="tiny"
            >
                <Modal.Content>
                    <Item>
                        <Item.Header>
                            LOGIN
                        </Item.Header>
                        <Item.Description>
                            <input type="text" placeholder="이메일을 입력하세요"/>
                            <input type="text" placeholder="비밀번호를 입력하세요"/>
                        </Item.Description>
                        <Item.Meta>
                            <Checkbox label={<label>로그인 유지</label>} />
                            <span>비밀번호를 잊으셨나요?</span>
                        </Item.Meta>
                    </Item>
                </Modal.Content>
                <Modal.Actions>
                <Button size="large" centered primary onClick={() => dispatch({ type: 'close' }) }>
                    LOGIN
                </Button>
                </Modal.Actions>
            </Modal>
            </>
        )
    }


export default SideBar;